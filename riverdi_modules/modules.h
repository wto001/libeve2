/*
 * Copyright (c) Riverdi Sp. z o.o. sp. k. <riverdi@riverdi.com>
 * Copyright (c) Skalski Embedded Technologies <contact@lukasz-skalski.com>
 */

#ifndef _MODULES_H_
#define _MODULES_H_

/*
 * Embedded Video Engine Series
 */

#if defined (EVE_1)
  #define FT80X_ENABLE
#elif defined (EVE_2)
  #define FT81X_ENABLE
#elif defined (EVE_3)
  #define BT81X_ENABLE
  #define FT81X_ENABLE
#else
  #error "Please choose generation of Embedded Video Engine (EVE_1, EVE_2, EVE_3)"
#endif

/* define the Display panel parameters here */
#if defined (EVE2_38) 
  #define DispWidth      480L
  #define DispHeight     116L

  #define DispHSize      480L
  #define DispVSize      272L
  #define DispHCycle     524L
  #define DispHOffset    43L
  #define DispHSync0     0L
  #define DispHSync1     41L
  #define DispVCycle     288L
  #define DispVOffset    12L
  #define DispVSync0     156L
  #define DispVSync1     10L
  #define DispPCLK       5
  #define DispSwizzle    0
  #define DispPCLKPol    1
  #define DispCSpread    1
  #define DispDither     1
#else
  #error "Please define timings in riverdi/modules.h file for custom displays"
  #define DispWidth      xxxL
  #define DispHeight     xxxL
  #define DispHCycle     xxxL
  #define DispHOffset    xxxL
  #define DispHSync0     xxxL
  #define DispHSync1     xxxL
  #define DispVCycle     xxxL
  #define DispVOffset    xxxL
  #define DispVSync0     xxxL
  #define DispVSync1     xxxL
  #define DispPCLK       x
  #define DispSwizzle    x
  #define DispPCLKPol    x
  #define DispCSpread    x
  #define DispDither     x
#endif

#endif /*_MODULES_H_*/
